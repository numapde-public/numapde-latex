\PassOptionsToPackage{fit}{truncate}
\ProvidesClass{numapde-exercise}[2020/04/17]

% Declare the options for the class
% Option noextras will suppress extra material (see numapde-teaching.sty)
\DeclareOption{noextras}{\PassOptionsToPackage{\CurrentOption}{numapde-teaching}}

% Option nosolutions will suppress solutions to exercises (see numapde-teaching.sty)
\DeclareOption{nosolutions}{\PassOptionsToPackage{\CurrentOption}{numapde-teaching}}

% Option final implies noextras, nosolutions (see numapde-teaching.sty)
\DeclareOption{final}{\PassOptionsToPackage{\CurrentOption}{numapde-teaching}}

% Option nosolutions will suppress solutions to exercises (see numapde-teaching.sty)
\DeclareOption{showfilenames}{\PassOptionsToPackage{\CurrentOption}{numapde-teaching}}

% Pass all non-implemented options to the base class scrartcl
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}

% Process all options
\ProcessOptions\relax

% Load the base class scrartcl with default documentclass option english
% https://tex.stackexchange.com/questions/147243
\RequirePackage{etoolbox}
\preto\@classoptionslist{english,}
\LoadClass[a4paper,DIV=12,parskip=full]{scrartcl}

% Resolve the dependencies of this package
\RequirePackage{numapde-cls-commons}
\RequirePackage{numapde-teaching}

% Configure the document class specific settings
% Set document type
\def\numapde@documenttype{\GetTranslation{Exercise}}

% Configure the problem counters for use in exercises
\counterwithout{numapde@problem}{section}
\counterwithout{numapde@homeworkproblem}{section}

% Set up the page header and footer
\addtokomafont{pageheadfoot}{\normalfont\sffamily\footnotesize}
\date{\ISOToday}
\if@twoside
	\ihead*{\numapde@shortauthor}
	\ohead*{\numapde@shorttitle}
	\cfoot*{\url{\numapde@courseurl}}
	\ofoot*{\GetTranslation{page}~\thepage~\GetTranslation{of}~\pageref*{LastPage}}
\else%
	\ihead*{\numapde@shortauthor}
	\ohead*{\numapde@shorttitle}
	\cfoot*{\url{\numapde@courseurl}}
	\ofoot*{\GetTranslation{page}~\thepage~\GetTranslation{of}~\pageref*{LastPage}}
\fi

\KOMAoptions{headlines=2}
\recalctypearea
