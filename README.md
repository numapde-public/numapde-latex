# Common LaTeX Resources
This repository collects common LaTeX Resources, see the [documentation](numapde-latex-documentation.pdf) for a comprehensive overview.

## One-Time Preparation (for numapde Group Members)
1. Clone this repository once.
   The recommended directory is `~/Work/Public/numapde-latex`.
2. Make sure this directory is contained in your `TEXINPUTS` environment variable.
   See https://www.tu-chemnitz.de/mathematik/numawiki/index.php/.profile.

## One-Time Preparation (for non-numapde Group Members)
You will normally be using this repository as a git submodule associated with a specific publication repository.
In that repository, you should be able to
```bash
source bin/numapde-set-paths.sh
```
which sets the environment variable `TEXINPUTS` for you.

# CI-Pipeline
The numapde ci updates the [documentation](numapde-latex-documentation.pdf) after every commit. We documented the configuration of the ci server at [numapde-bibliography](https://gitlab.hrz.tu-chemnitz.de/numapde-public/numapde-bibliography).

## Runner registration per tag (or task?)
The runner requires a [registration](https://docs.gitlab.com/runner/register/index.html) per project and tag. The command on **cicd.mathematik.tu-chemnitz.de**
```
PROJECTID=5272 # https://gitlab.hrz.tu-chemnitz.de/numapde-public/numapde-latex
TOKEN=$(curl --silent --header "Private-Token: ${NUMAPDE_GITLAB_TOKEN}" https://${NUMAPDE_GITLAB_SERVER}/api/v4/projects/${PROJECTID} | awk 'BEGIN {RS=","; FS="\""} /runners_token/ {print $4}')
ssh ${NUMAPDE_MRZ_LOGIN}@${NUMAPDE_GITLAB_CISERVER} sudo gitlab-runner register --non-interactive --limit 0 --request-concurrency 0 --url "https://${NUMAPDE_GITLAB_SERVER}" --executor "shell" --name "numapde-gitlab-runner" --registration-token "$TOKEN" --tag-list "update-documentation"
```
registers a runner with tag `update-documentation`. 
 
