\PassOptionsToPackage{fit}{truncate}
\ProvidesClass{numapde-lecture-notes}[2020/03/18]
% Part of this work has been inspired by the jnsao.cls file
% from http://jnsao.episciences.org

% Declare the options for the class
% Option noextras will suppress extra material (see numapde-teaching.sty)
\DeclareOption{noextras}{\PassOptionsToPackage{\CurrentOption}{numapde-teaching}}

% Option nosolutions will suppress solutions to exercises (see numapde-teaching.sty)
\DeclareOption{nosolutions}{\PassOptionsToPackage{\CurrentOption}{numapde-teaching}}

% Option final implies noextras, nosolutions (see numapde-teaching.sty)
\DeclareOption{final}{\PassOptionsToPackage{\CurrentOption}{numapde-teaching}}

% Pass all non-implemented options to the base class scrreprt
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrreprt}}

% Process all options
\ProcessOptions\relax

% Load the base class scrreprt with default documentclass option english
% https://tex.stackexchange.com/questions/147243/a-class-with-default-language-settings
\RequirePackage{etoolbox}
\preto\@classoptionslist{english,}
\LoadClass[a4paper,DIV=12,parskip=full]{scrreprt}

% Resolve the dependencies of this package
\RequirePackage{numapde-cls-commons}
\RequirePackage{numapde-teaching}

% Configure the document class specific settings
% Set document type
\def\numapde@documenttype{\GetTranslation{Lecture Notes}}

% Remove \@date and spacing following it from \@maketitle
% https://tex.stackexchange.com/questions/288800/omit-the-date-in-maketitle-without-blanking-the-date
\patchcmd{\@maketitle}{{\usekomafont{date}{\@date \par}}\vskip \z@ \@plus 1em}{}{}{}

% Set up the page header and footer
\addtokomafont{pageheadfoot}{\normalfont\sffamily\footnotesize}
\date{\ISOToday}
\if@twoside
	\rohead[]{\truncate{1.0\textwidth}{\ifx\numapde@shorttitle\empty{}\else{\numapde@shorttitle}\fi}}
	\cohead[]{\numapde@license}
	\cehead[]{\numapde@license}
	\lehead[]{\truncate{1.0\textwidth}{\numapde@shortauthor}}
	\refoot[]{\ISOToday}
	\cfoot*{\url{\numapde@courseurl}}
	\ofoot*{\GetTranslation{page}~\thepage~\GetTranslation{of}~\pageref*{LastPage}}
\else
	\ohead[]{\truncate{0.45\textwidth}{\ifx\numapde@shorttitle\empty{}\else{\numapde@shorttitle}\fi}}
	\chead[]{\numapde@license}
	\ihead[]{\truncate{0.45\textwidth}{\numapde@shortauthor}}
	\ofoot*{\GetTranslation{page}~\thepage~\GetTranslation{of}~\pageref*{LastPage}}
	\cfoot*{\url{\numapde@courseurl}}
	\ifoot*{\ISOToday}
\fi
\pagestyle{scrheadings}
\KOMAoptions{headsepline=true}
\KOMAoptions{footsepline=true}
\KOMAoptions{plainfootsepline=true}

% Change formatting, counting of sections
% section without chapter prefix and independently numbered, as well as prefixed §
% \RedeclareSectionCommand[counterwithout=chapter]{section}
\counterwithout{section}{chapter}
\renewcommand{\chapterformat}{\GetTranslation{Chapter}~\thechapter\hspace{1em}}
\renewcommand{\sectionformat}{§~\arabic{section}\hspace{1em}}
\renewcommand{\subsectionformat}{§~\arabic{section}.\arabic{subsection}\hspace{1em}}
\renewcommand{\subsubsectionformat}{§~\arabic{section}.\arabic{subsection}.\arabic{subsubsection}\hspace{1em}}
