\PassOptionsToPackage{fit}{truncate}
\ProvidesClass{numapde-dfg-proposal}[2020/08/14]
% Part of this work has been inspired by the jnsao.cls file
% from http://jnsao.episciences.org

% Pass all non-implemented options to the base class scrartcl
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}

% Process all options
\ProcessOptions\relax

% Load the base class scrartcl with default documentclass option english
% https://tex.stackexchange.com/questions/147243/a-class-with-default-language-settings
\RequirePackage{etoolbox}
\preto\@classoptionslist{english,}
\LoadClass[a4paper,DIV=12,parskip=half]{scrartcl}

% Resolve the dependencies of this package
\RequirePackage{numapde-cls-commons}
\RequirePackage{numapde-dfg-proposal}

% Configure the document class specific settings
% Set document type
\def\numapde@documenttype{\GetTranslation{DFG Proposal}}

% Remove \@date and spacing following it from \@maketitle
% https://tex.stackexchange.com/questions/288800/omit-the-date-in-maketitle-without-blanking-the-date
\patchcmd{\@maketitle}{{\usekomafont{date}{\@date \par}}\vskip \z@ \@plus 1em}{}{}{}

% Set up the page header and footer
\RequirePackage{truncate}
\if@twoside
	\rohead[]{\truncate{1.0\textwidth}{\ifx\numapde@shorttitle\empty{}\else{\numapde@shorttitle}\fi}}
	\lehead[]{\truncate{1.0\textwidth}{\numapde@shortauthor}}
	\refoot[]{\ISOToday}
	\lofoot[]{\numapde@license}
	\ofoot[]{page~\thepage~of~\pageref*{LastPage}}
\else
	\ohead[]{\truncate{0.45\textwidth}{\ifx\numapde@shorttitle\empty{}\else{\numapde@shorttitle}\fi}}
	\ihead[]{\truncate{0.45\textwidth}{\numapde@shortauthor}}
	\ofoot*{page~\thepage~of~\pageref*{LastPage}}
	\cfoot[]{\numapde@license}
	\ifoot*{\ISOToday}
\fi
\pagestyle{scrheadings}
\KOMAoptions{headsepline=true}
\KOMAoptions{footsepline=true}
\KOMAoptions{plainfootsepline=true}

