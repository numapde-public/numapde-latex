% TODO why the double brackets?
% This LaTeX package provides numerous short-hand, low-level commands
% which do not carry a semantic meaning. The latter are defined in
% numapde-semantic.sty.
\ProvidesPackage{numapde-syntax}

% Resolve the dependencies of this package
\RequirePackage{numapde-packages}
\RequirePackage{calc}

% Wiley class articles load stix.sty, which provides its own version 
% of \mathscr
\@ifclassloaded{WileyNJD-v2}{}{%
	\RequirePackage{mathrsfs}
}

% Define commands for bold upper-case letters in math mode
\newcommand{\bA}{{\boldsymbol{A}}}
\newcommand{\bB}{{\boldsymbol{B}}}
\newcommand{\bC}{{\boldsymbol{C}}}
\newcommand{\bD}{{\boldsymbol{D}}}
\newcommand{\bE}{{\boldsymbol{E}}}
\newcommand{\bF}{{\boldsymbol{F}}}
\newcommand{\bG}{{\boldsymbol{G}}}
\newcommand{\bH}{{\boldsymbol{H}}}
\newcommand{\bI}{{\boldsymbol{I}}}
\newcommand{\bJ}{{\boldsymbol{J}}}
\newcommand{\bK}{{\boldsymbol{K}}}
\newcommand{\bL}{{\boldsymbol{L}}}
\newcommand{\bM}{{\boldsymbol{M}}}
\newcommand{\bN}{{\boldsymbol{N}}}
\newcommand{\bO}{{\boldsymbol{O}}}
\newcommand{\bP}{{\boldsymbol{P}}}
\newcommand{\bQ}{{\boldsymbol{Q}}}
\newcommand{\bR}{{\boldsymbol{R}}}
\newcommand{\bS}{{\boldsymbol{S}}}
\newcommand{\bT}{{\boldsymbol{T}}}
\newcommand{\bU}{{\boldsymbol{U}}}
\newcommand{\bV}{{\boldsymbol{V}}}
\newcommand{\bW}{{\boldsymbol{W}}}
\newcommand{\bX}{{\boldsymbol{X}}}
\newcommand{\bY}{{\boldsymbol{Y}}}
\newcommand{\bZ}{{\boldsymbol{Z}}}

% Define commands for bold lower-case letters in math mode
\newcommand{\ba}{{\boldsymbol{a}}}
\newcommand{\bb}{{\boldsymbol{b}}}
\newcommand{\bc}{{\boldsymbol{c}}}
\newcommand{\bd}{{\boldsymbol{d}}}
\newcommand{\be}{{\boldsymbol{e}}}
\let\bf\undefined \newcommand{\bf}{{\boldsymbol{f}}}
\newcommand{\bg}{{\boldsymbol{g}}}
\newcommand{\bh}{{\boldsymbol{h}}}
\let\bi\undefined \newcommand{\bi}{{\boldsymbol{i}}}  % Taylor and Francis
\newcommand{\bj}{{\boldsymbol{j}}}
\newcommand{\bk}{{\boldsymbol{k}}}
\newcommand{\bl}{{\boldsymbol{l}}}
\let\bm\undefined \newcommand{\bm}{{\boldsymbol{m}}}  % Taylor and Francis
\newcommand{\bn}{{\boldsymbol{n}}}
\newcommand{\bo}{{\boldsymbol{o}}}
\newcommand{\bp}{{\boldsymbol{p}}}
\newcommand{\bq}{{\boldsymbol{q}}}
\newcommand{\br}{{\boldsymbol{r}}}
\newcommand{\bs}{{\boldsymbol{s}}}
\newcommand{\bt}{{\boldsymbol{t}}}
\newcommand{\bu}{{\boldsymbol{u}}}
\newcommand{\bv}{{\boldsymbol{v}}}
\newcommand{\bw}{{\boldsymbol{w}}}
\newcommand{\bx}{{\boldsymbol{x}}}
\newcommand{\by}{{\boldsymbol{y}}}
\newcommand{\bz}{{\boldsymbol{z}}}

% Define commands for bold numbers 0, 1 in math mode
\newcommand{\bnull}{{\boldsymbol{0}}}
\newcommand{\bone}{{\boldsymbol{1}}}

% Define commands for bold lower-case Greek letters in math mode
\newcommand{\balpha}{{\boldsymbol{\alpha}}}
\newcommand{\bbeta}{{\boldsymbol{\beta}}}
\newcommand{\bgamma}{{\boldsymbol{\gamma}}}
\newcommand{\bdelta}{{\boldsymbol{\delta}}}
\newcommand{\bepsilon}{{\boldsymbol{\epsilon}}}
\newcommand{\bvarepsilon}{{\boldsymbol{\varepsilon}}}
\newcommand{\bzeta}{{\boldsymbol{\zeta}}}
\newcommand{\boldeta}{{\boldsymbol{\eta}}}  % exception: \boldeta instead of \beta
\newcommand{\btheta}{{\boldsymbol{\theta}}}
\newcommand{\bvartheta}{{\boldsymbol{\vartheta}}}
\newcommand{\biota}{{\boldsymbol{\iota}}}
\newcommand{\bkappa}{{\boldsymbol{\kappa}}}
\newcommand{\bvarkappa}{{\boldsymbol{\varkappa}}}
\newcommand{\blambda}{{\boldsymbol{\lambda}}}
\newcommand{\bmu}{{\boldsymbol{\mu}}}
\newcommand{\bnu}{{\boldsymbol{\nu}}}
\newcommand{\bomicron}{\boldsymbol{o}}
\newcommand{\bxi}{{\boldsymbol{\xi}}}
\newcommand{\bpi}{{\boldsymbol{\pi}}}
\newcommand{\bvarpi}{{\boldsymbol{\varpi}}}
\newcommand{\brho}{{\boldsymbol{\rho}}}
\newcommand{\bvarrho}{{\boldsymbol{\varrho}}}
\newcommand{\bsigma}{{\boldsymbol{\sigma}}}
\newcommand{\bvarsigma}{{\boldsymbol{\varsigma}}}
\newcommand{\btau}{{\boldsymbol{\tau}}}
\newcommand{\bupsilon}{{\boldsymbol{\upsilon}}}
\newcommand{\bphi}{{\boldsymbol{\phi}}}
\newcommand{\bvarphi}{{\boldsymbol{\varphi}}}
\newcommand{\bchi}{{\boldsymbol{\chi}}}
\newcommand{\bpsi}{{\boldsymbol{\psi}}}
\newcommand{\bomega}{{\boldsymbol{\omega}}}

% Define commands for bold upper-case Greek letters in math mode
\newcommand{\bAlpha}{{\boldsymbol{A}}}
\newcommand{\bBeta}{{\boldsymbol{B}}}
\newcommand{\bGamma}{{\boldsymbol{\Gamma}}}
\newcommand{\bDelta}{{\boldsymbol{\Delta}}}
\newcommand{\bEpsilon}{{\boldsymbol{E}}}
\newcommand{\bZeta}{{\boldsymbol{Z}}}
\newcommand{\bEta}{{\boldsymbol{H}}}
\newcommand{\bTheta}{{\boldsymbol{\Theta}}}
\newcommand{\bIota}{{\boldsymbol{I}}}
\newcommand{\bKappa}{{\boldsymbol{K}}}
\newcommand{\bLambda}{{\boldsymbol{\Lambda}}}
\newcommand{\bMu}{{\boldsymbol{M}}}
\newcommand{\bNu}{{\boldsymbol{N}}}
\newcommand{\bXi}{{\boldsymbol{\Xi}}}
\newcommand{\bOmicron}{{\boldsymbol{O}}}
\newcommand{\bPi}{{\boldsymbol{\Pi}}}
\newcommand{\bRho}{{\boldsymbol{P}}}
\newcommand{\bSigma}{{\boldsymbol{\Sigma}}}
\newcommand{\bTau}{{\boldsymbol{T}}}
\newcommand{\bUpsilon}{{\boldsymbol{\Upsilon}}}
\newcommand{\bPhi}{{\boldsymbol{\Phi}}}
\newcommand{\bChi}{{\boldsymbol{X}}}
\newcommand{\bPsi}{{\boldsymbol{\Psi}}}
\newcommand{\bOmega}{{\boldsymbol{\Omega}}}

% Define commands for calligraphic upper-case letters in math mode
\newcommand{\cA}{\mathcal{A}}
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cC}{\mathcal{C}}
\newcommand{\cD}{\mathcal{D}}
\newcommand{\cE}{\mathcal{E}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\cG}{\mathcal{G}}
\newcommand{\cH}{\mathcal{H}}
\newcommand{\cI}{\mathcal{I}}
\newcommand{\cJ}{\mathcal{J}}
\newcommand{\cK}{\mathcal{K}}
\newcommand{\cL}{\mathcal{L}}
\newcommand{\cM}{\mathcal{M}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cP}{\mathcal{P}}
\newcommand{\cQ}{\mathcal{Q}}
\newcommand{\cR}{\mathcal{R}}
\newcommand{\cS}{\mathcal{S}}
\newcommand{\cT}{\mathcal{T}}
\newcommand{\cU}{\mathcal{U}}
\newcommand{\cV}{\mathcal{V}}
\newcommand{\cW}{\mathcal{W}}
\newcommand{\cX}{\mathcal{X}}
\newcommand{\cY}{\mathcal{Y}}
\newcommand{\cZ}{\mathcal{Z}}

% Define commands for fraktur upper-case letters in math mode
\newcommand{\fA}{\mathfrak{A}}
\newcommand{\fB}{\mathfrak{B}}
\newcommand{\fC}{\mathfrak{C}}
\newcommand{\fD}{\mathfrak{D}}
\newcommand{\fE}{\mathfrak{E}}
\newcommand{\fF}{\mathfrak{F}}
\newcommand{\fG}{\mathfrak{G}}
\newcommand{\fH}{\mathfrak{H}}
\newcommand{\fI}{\mathfrak{I}}
\newcommand{\fJ}{\mathfrak{J}}
\newcommand{\fK}{\mathfrak{K}}
\newcommand{\fL}{\mathfrak{L}}
\newcommand{\fM}{\mathfrak{M}}
\newcommand{\fN}{\mathfrak{N}}
\newcommand{\fO}{\mathfrak{O}}
\newcommand{\fP}{\mathfrak{P}}
\newcommand{\fQ}{\mathfrak{Q}}
\newcommand{\fR}{\mathfrak{R}}
\newcommand{\fS}{\mathfrak{S}}
\newcommand{\fT}{\mathfrak{T}}
\newcommand{\fU}{\mathfrak{U}}
\newcommand{\fV}{\mathfrak{V}}
\newcommand{\fW}{\mathfrak{W}}
\newcommand{\fX}{\mathfrak{X}}
\newcommand{\fY}{\mathfrak{Y}}
\newcommand{\fZ}{\mathfrak{Z}}


% Define commands for fraktur upper-case letters in math mode
\newcommand{\sA}{\mathscr{A}}
\newcommand{\sB}{\mathscr{B}}
\newcommand{\sC}{\mathscr{C}}
\newcommand{\sD}{\mathscr{D}}
\newcommand{\sE}{\mathscr{E}}
\newcommand{\sF}{\mathscr{F}}
\newcommand{\sG}{\mathscr{G}}
\newcommand{\sH}{\mathscr{H}}
\newcommand{\sI}{\mathscr{I}}
\newcommand{\sJ}{\mathscr{J}}
\newcommand{\sK}{\mathscr{K}}
\newcommand{\sL}{\mathscr{L}}
\newcommand{\sM}{\mathscr{M}}
\newcommand{\sN}{\mathscr{N}}
\newcommand{\sO}{\mathscr{O}}
\newcommand{\sP}{\mathscr{P}}
\newcommand{\sQ}{\mathscr{Q}}
\newcommand{\sR}{\mathscr{R}}
\newcommand{\sS}{\mathscr{S}}
\newcommand{\sT}{\mathscr{T}}
\newcommand{\sU}{\mathscr{U}}
\newcommand{\sV}{\mathscr{V}}
\newcommand{\sW}{\mathscr{W}}
\newcommand{\sX}{\mathscr{X}}
\newcommand{\sY}{\mathscr{Y}}
\newcommand{\sZ}{\mathscr{Z}}

% Define commands for lower-case letters with vector accents in math mode
\newcommand{\va}{\vec{a}}
\newcommand{\vb}{\vec{b}}
\newcommand{\vc}{\vec{c}}
\newcommand{\vd}{\vec{d}}
\newcommand{\ve}{\vec{e}}
\newcommand{\vf}{\vec{f}}
\let\vg\undefined \newcommand{\vg}{\vec{g}}  % ESAIM
\newcommand{\vh}{\vec{h}}
\newcommand{\vi}{\vec{i}}
\newcommand{\vj}{\vec{j}}
\newcommand{\vk}{\vec{k}}
\newcommand{\vl}{\vec{l}}
\newcommand{\vm}{\vec{m}}
\newcommand{\vn}{\vec{n}}
\newcommand{\vo}{\vec{o}}
\newcommand{\vp}{\vec{p}}
\newcommand{\vq}{\vec{q}}
\newcommand{\vr}{\vec{r}}
\newcommand{\vs}{\vec{s}}
\newcommand{\vt}{\vec{t}}
\newcommand{\vu}{\vec{u}}
\let\vv\undefined \newcommand{\vv}{\vec{v}}  % jnsao
\newcommand{\vw}{\vec{w}}
\newcommand{\vx}{\vec{x}}
\newcommand{\vy}{\vec{y}}
\newcommand{\vz}{\vec{z}}

% Define commands for upper-case letters with vector accents in math mode
\newcommand{\vA}{\vec{A}}
\newcommand{\vB}{\vec{B}}
\newcommand{\vC}{\vec{C}}
\newcommand{\vD}{\vec{D}}
\newcommand{\vE}{\vec{E}}
\newcommand{\vF}{\vec{F}}
\newcommand{\vG}{\vec{G}}
\newcommand{\vH}{\vec{H}}
\newcommand{\vI}{\vec{I}}
\newcommand{\vJ}{\vec{J}}
\newcommand{\vK}{\vec{K}}
\newcommand{\vL}{\vec{L}}
\newcommand{\vM}{\vec{M}}
\newcommand{\vN}{\vec{N}}
\newcommand{\vO}{\vec{O}}
\newcommand{\vP}{\vec{P}}
\newcommand{\vQ}{\vec{Q}}
\newcommand{\vR}{\vec{R}}
\newcommand{\vS}{\vec{S}}
\newcommand{\vT}{\vec{T}}
\newcommand{\vU}{\vec{U}}
\newcommand{\vV}{\vec{V}}
\newcommand{\vW}{\vec{W}}
\newcommand{\vX}{\vec{X}}
\newcommand{\vY}{\vec{Y}}
\newcommand{\vZ}{\vec{Z}}

% Define commands for numbers 0, 1 with vector accents in math mode
\newcommand{\vnull}{{\vec{0}}}
\newcommand{\vone}{{\vec{1}}}

% Define commands for lower-case Greek letters with vector accents in math mode
\newcommand{\valpha}{{\vec{\alpha}}}
\newcommand{\vbeta}{{\vec{\beta}}}
\newcommand{\vgamma}{{\vec{\gamma}}}
\newcommand{\vdelta}{{\vec{\delta}}}
\newcommand{\vepsilon}{{\vec{\epsilon}}}
\newcommand{\vvarepsilon}{{\vec{\varepsilon}}}
\newcommand{\vzeta}{{\vec{\zeta}}}
\newcommand{\veta}{{\vec{\eta}}}
\newcommand{\vtheta}{{\vec{\theta}}}
\newcommand{\vvartheta}{{\vec{\vartheta}}}
\newcommand{\viota}{{\vec{\iota}}}
\newcommand{\vkappa}{{\vec{\kappa}}}
\newcommand{\vvarkappa}{{\vec{\varkappa}}}
\newcommand{\vlambda}{{\vec{\lambda}}}
\newcommand{\vmu}{{\vec{\mu}}}
\newcommand{\vnu}{{\vec{\nu}}}
\newcommand{\vomicron}{\vec{o}}
\newcommand{\vxi}{{\vec{\xi}}}
\newcommand{\vpi}{{\vec{\pi}}}
\newcommand{\vvarpi}{{\vec{\varpi}}}
\newcommand{\vrho}{{\vec{\rho}}}
\newcommand{\vvarrho}{{\vec{\varrho}}}
\newcommand{\vsigma}{{\vec{\sigma}}}
\newcommand{\vvarsigma}{{\vec{\varsigma}}}
\newcommand{\vtau}{{\vec{\tau}}}
\newcommand{\vupsilon}{{\vec{\upsilon}}}
\newcommand{\vphi}{{\vec{\phi}}}
\newcommand{\vvarphi}{{\vec{\varphi}}}
\newcommand{\vchi}{{\vec{\chi}}}
\newcommand{\vpsi}{{\vec{\psi}}}
\newcommand{\vomega}{{\vec{\omega}}}

% Define commands for upper-case Greek letters with vector accents in math mode
\newcommand{\vAlpha}{{\vec{A}}}
\newcommand{\vBeta}{{\vec{B}}}
\newcommand{\vGamma}{{\vec{\Gamma}}}
\newcommand{\vDelta}{{\vec{\Delta}}}
\newcommand{\vEpsilon}{{\vec{E}}}
\newcommand{\vZeta}{{\vec{Z}}}
\newcommand{\vEta}{{\vec{H}}}
\newcommand{\vTheta}{{\vec{\Theta}}}
\newcommand{\vIota}{{\vec{I}}}
\newcommand{\vKappa}{{\vec{K}}}
\newcommand{\vLambda}{{\vec{\Lambda}}}
\newcommand{\vMu}{{\vec{M}}}
\newcommand{\vNu}{{\vec{N}}}
\newcommand{\vOmicron}{\vec{O}}
\newcommand{\vXi}{{\vec{\Xi}}}
\newcommand{\vPi}{{\vec{\Pi}}}
\newcommand{\vRho}{{\vec{P}}}
\newcommand{\vSigma}{{\vec{\Sigma}}}
\newcommand{\vTau}{{\vec{T}}}
\newcommand{\vUpsilon}{{\vec{\Upsilon}}}
\newcommand{\vPhi}{{\vec{\Phi}}}
\newcommand{\vChi}{{\vec{X}}}
\newcommand{\vPsi}{{\vec{\Psi}}}
\newcommand{\vOmega}{{\vec{\Omega}}}

% Define commands for some blackboard characters in math mode, mainly used for sets
\let\C\undefined \newcommand{\C}{\mathbb{C}}
\newcommand{\K}{\mathbb{K}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\R}{\mathbb{R}}
\let\S\undefined \newcommand{\S}{\mathbb{S}}
\newcommand{\Z}{\mathbb{Z}}

% Define some English abbreviations
\let\aa\undefined \newcommand{\aa}{a.a.\xspace}
\newcommand{\ale}{a.e.\xspace}
\let\cf\undefined \newcommand{\cf}{cf.\xspace}
\newcommand{\eg}{e.\,g.\xspace}
\newcommand{\Eg}{E.\,g.\xspace}
\let\ie\undefined \newcommand{\ie}{i.\,e.\xspace}
\newcommand{\Ie}{I.\,e.\xspace}
\newcommand{\iid}{i.\,i.\,d.\xspace}
\let\st\undefined \newcommand{\st}{s.\,t.\xspace}  % WileyNJD-v2.cls (loads soul.sty)
\newcommand{\wolog}{w.l.o.g.\xspace} % \wlog is a LaTeX core command which writes to the .log file
\newcommand{\wrt}{w.r.t.\xspace}

% Define some German abbreviations
\newcommand{\bspw}{bspw.\xspace}
\newcommand{\bzgl}{bzgl.\xspace}
\newcommand{\bzw}{bzw.\xspace}
\newcommand{\dah}{d.\,h.\xspace}
\newcommand{\Dah}{D.\,h.\xspace}
\let\etc\undefined \newcommand{\etc}{etc.\xspace} % cocv.cls
\newcommand{\evtl}{evtl.\xspace}
\newcommand{\Evtl}{Evtl.\xspace}
\newcommand{\fue}{f.\,ü.\xspace}
\newcommand{\fs}{f.\,s.\xspace}
\newcommand{\iA}{i.\,A.\xspace}
\newcommand{\IA}{I.\,A.\xspace}
\newcommand{\idR}{i.\,d.\,R.\xspace}
\newcommand{\IdR}{I.\,d.\,R.\xspace}
\newcommand{\iW}{i.\,W.\xspace}
\newcommand{\IW}{I.\,W.\xspace}
\newcommand{\mE}{m.\,E.\xspace}
\newcommand{\oBdA}{o.\,B.\,d.\,A.\xspace}
\newcommand{\OBdA}{O.\,B.\,d.\,A.\xspace}
\let\og\undefined \newcommand{\og}{o.\,g.\xspace}  % cedram.cls
\newcommand{\oae}{o.\,ä.\xspace}
\newcommand{\pa}{p.\,a.\xspace}
\newcommand{\spd}{s.\,p.\,d.\xspace}
\newcommand{\ua}{u.\,a.\xspace}
\newcommand{\ug}{u.\,g.\xspace}
\newcommand{\usw}{usw.\xspace}
\newcommand{\Ua}{U.\,a.\xspace}
\newcommand{\uU}{u.\,U.\xspace}
\newcommand{\UnU}{U.\,U.\xspace}
\newcommand{\vgl}{vgl.\xspace}
\newcommand{\zB}{z.\,B.\xspace}
\newcommand{\ZB}{Z.\,B.\xspace}
\newcommand{\zHd}{z.\,Hd.\xspace}

% Define a default markdown for named (mostly software related)
% packages, and some commands for often-used packages as well
\newcommand{\namemd}[1]{\textsc{#1}\xspace}
\newcommand{\adimat}{\namemd{ADiMat}}
\newcommand{\ampl}{\namemd{AMPL}}
\newcommand{\BibTeX}{\namemd{Bib\TeX}}
\newcommand{\BibLaTeX}{\namemd{Bib\LaTeX}}
\newcommand{\cg}{\namemd{CG}}
\newcommand{\cpp}{\namemd{C++}}
\newcommand{\cppmat}{\namemd{cppmat}}
\newcommand{\dolfin}{\namemd{Dolfin}}
\newcommand{\dolfinx}{\namemd{DOLFINx}}
\newcommand{\dolfinplot}{\namemd{Dolfin-Plot}}
\newcommand{\dolfinadjoint}{\namemd{Dolfin-Adjoint}}
\newcommand{\doxygen}{\namemd{Doxygen}}
\newcommand{\femorph}{\namemd{FEMorph}}
\newcommand{\fenics}{\namemd{FEniCS}}
\newcommand{\ffc}{\namemd{FFC}}
\newcommand{\fmg}{\namemd{FMG}}
\newcommand{\fortran}{\namemd{Fortran}}
\newcommand{\gitlab}{\namemd{GitLab}}
\newcommand{\gmres}{\namemd{Gmres}}
\newcommand{\gmsh}{\namemd{Gmsh}}
\newcommand{\ipopt}{\namemd{Ipopt}}
\newcommand{\libsvm}{\namemd{LIBSVM}}
\newcommand{\liblinear}{\namemd{LIBLINEAR}}
\newcommand{\macmpec}{\namemd{MacMPEC}}
\newcommand{\manifoldsjl}{\namemd{Manifolds.jl}}
\newcommand{\manopt}{\namemd{Manopt}}
\newcommand{\manoptjl}{\namemd{Manopt.jl}}
\newcommand{\mathematica}{\namemd{Mathematica}}
\newcommand{\matlab}{\namemd{Matlab}}
\newcommand{\maple}{\namemd{Maple}}
\newcommand{\maxima}{\namemd{Maxima}}
\newcommand{\meshio}{\namemd{meshio}}
\newcommand{\metis}{\namemd{Metis}}
\newcommand{\minres}{\namemd{Minres}}
\newcommand{\mshr}{\namemd{mshr}}
\newcommand{\mvirt}{\namemd{MVIRT}}
\newcommand{\numapde}{\namemd{numapde}}
\newcommand{\numpy}{\namemd{NumPy}}
\newcommand{\paraview}{\namemd{Paraview}}
\newcommand{\pdflatex}{\namemd{PDF\LaTeX}}
\newcommand{\perl}{\namemd{Perl}}
\newcommand{\petsc}{\namemd{PETSc}}
\newcommand{\pymat}{\namemd{pymat}}
\newcommand{\python}{\namemd{Python}}
\newcommand{\scikit}{\namemd{SciKit}}
\newcommand{\scikitlearn}{\namemd{SciKit-learn}}
\newcommand{\scipy}{\namemd{SciPy}}
\newcommand{\sphinx}{\namemd{Sphinx}}
\newcommand{\subgmres}{\namemd{SubGmres}}
\newcommand{\subminres}{\namemd{SubMinres}}
\newcommand{\superlu}{\namemd{SuperLU}}
\newcommand{\svmlight}{\namemd{SVM${}^\text{light}$}}
\newcommand{\TikZ}{Ti\textit{k}Z\xspace}
\newcommand{\tritetmesh}{\namemd{TriTetMesh}}
\newcommand{\ufl}{\namemd{UFL}}
\newcommand{\uqlab}{\namemd{UQLab}}
\newcommand{\viper}{\namemd{Viper}}
\newcommand{\xml}{\namemd{XML}}

% Define \enclose command
% \enclose[a]{b}{c}{d} encloses content (c) in brackets (b,d) where the optional parameter
% a scales the brackets (big, Big, bigg, Bigg) or (auto)matically scales them.
% It can also be set to [none] do deactivate the brackets/enclosing
% \enclosespacing allows for additional spacing before and after the \enclose:d content
% https://blag.nullteilerfrei.de/2014/01/16/a-dynamic-bracketing-macro-in-latex/
% The \enclose command is mainly meant for internal use; use \paren instead
\newcommand{\enclspacing}{}
\newcommand{\enclose}[4][]{%
	\ifthenelse{\isempty{#1}}%
	{\ifthenelse{\equal{#2}{.}}{}{#2}\enclspacing#3\enclspacing#4}%
	{\ifthenelse{\equal{#1}{auto}}% given auto
		{\mathopen{}\left#2\enclspacing#3\enclspacing\mathclose{}\right#4}%
		{\ifthenelse{\equal{#1}{none}}% given none
			{#3}%
			{\csname#1l\endcsname#2\enclspacing#3\enclspacing\csname#1r\endcsname#4}%
		}% end auto
	}% end #1 empty
}

% Define \encloseSet command to do { c | e }
% \encloseSet[a]{b}{c}{d}{e}{f} encloses content c and e in brackets (b,f) with a center delimiter d, where the optional parameter
% a scales the brackets (big, Big, bigg, Bigg) or (auto)matically scales them
% \enclosespacingSet allows for additional spacing before and after the center delimiter, the outer brackets gain a \encloseSpacing as \enclose:
% https://blag.nullteilerfrei.de/2014/01/16/a-dynamic-bracketing-macro-in-latex/
% The \enclose command is mainly meant for internal use; use \paren instead
\newcommand{\enclspacingSet}{\,}
\newcommand{\encloseSet}[6][]{%
	\ifthenelse{\isempty{#1}}%
		{#2\enclspacing#3 \enclspacingSet#4\enclspacingSet #5\enclspacing#6}%
		{\ifthenelse{\equal{#1}{auto}}%
			{\left#2\enclspacing#3\enclspacingSet\middle#4\enclspacingSet#5\right#6}%
			{\csname#1l\endcsname#2\enclspacing#3\enclspacingSet\csname#1\endcsname#4\enclspacingSet#5\enclspacing\csname#1r\endcsname#6}%
		}%
	}

% Define \paren command
% \paren[a]bc{d} encloses content (d) in brackets b,c where the optional parameter
% a scales the brackets
\newcommand{\paren}[4][]{\enclose[#1]{#2}{#4}{#3}}

% Define the \clap (complementing \{l,r}lap) and \math{l,c,r}lap commands,
% which horizontally smash the argument with alignment
% http://www.tug.org/TUGboat/Articles/tb22-4/tb72perlS.pdf
\def\clap#1{\hbox to 0pt{\hss#1\hss}}
\def\mathllap{\mathpalette\mathllapinternal}
\def\mathrlap{\mathpalette\mathrlapinternal}
\def\mathclap{\mathpalette\mathclapinternal}
\def\mathllapinternal#1#2{\llap{$\mathsurround=0pt#1{#2}$}}
\def\mathrlapinternal#1#2{\rlap{$\mathsurround=0pt#1{#2}$}}
\def\mathclapinternal#1#2{\clap{$\mathsurround=0pt#1{#2}$}}

% Define the \mrep[a]{b}{c} command, which prints b but reserves the space for c.
% The optional argument a specifies the alignment {l,c,r}. The command works in text and math modes.
% https://groups.google.com/forum/?fromgroups#!topic/comp.text.tex/GikFchXJIM0
\def\my@mathpalette#1#2#3#4{\mathchoice{#1\displaystyle{#2}{#3}{#4}}{#1\textstyle{#2}{#3}{#4}}{#1\scriptstyle{#2}{#3}{#4}}{#1\scriptscriptstyle{#2}{#3}{#4}}}
\def\mrepinternal#1#2#3#4{\makebox[\widthof{$#1#4$}][#2]{$#1\vphantom{#4}{#3}$}}
\newcommand{\mrep}[3][l]{\ifmmode\my@mathpalette\mrepinternal{#1}{#2}{#3}\else\makebox[\widthof{#3}][#1]{\vphantom{#3}#2}\fi}

% Simplify the use of German and English quotation marks
\newcommand{\gq}[1]{\glq#1\grq}
\newcommand{\gqq}[1]{\glqq#1\grqq}
\newcommand{\eq}[1]{`#1'}
\newcommand{\eqq}[1]{``#1''}
